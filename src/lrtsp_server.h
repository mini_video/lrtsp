#ifndef __LRTSP_RERVER_H__
#define __LRTSP_RERVER_H__


int lrtsp_server_init(const char* host, const char* port);
int lrtsp_server_deinit(void);

int lrtsp_server_run(void);


#endif /*__LRTSP_RERVER_H__*/